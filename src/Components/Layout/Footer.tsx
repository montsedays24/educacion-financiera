import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { Box, IconButton, useMediaQuery } from '@mui/material';
import FacebookIcon from '@mui/icons-material/Facebook';
import TwitterIcon from '@mui/icons-material/Twitter';
import InstagramIcon from '@mui/icons-material/Instagram';
// Google fonts
<link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet"/>

function Footer() {

const smallScreen = useMediaQuery('(max-width:600px)');
    return (
        <AppBar position="fixed" sx={{ top: 'auto', bottom: 0, bgcolor: 'black' }}>
            <Container maxWidth="xl">
                <Toolbar disableGutters sx={{ justifyContent: 'space-between' }}>
                    <Typography variant="body2" color="white">
                        {"Copyright © "}MiApp{new Date().getFullYear()}{"."}
                    </Typography>        
                    <Box sx={{ display: 'flex', alignItems: 'center' }}>
                        <IconButton color="inherit" href="https://facebook.com" target="_blank">
                            <FacebookIcon />
                        </IconButton>
                        <IconButton color="inherit" href="https://twitter.com" target="_blank">
                            <TwitterIcon />
                        </IconButton>
                        <IconButton color="inherit" href="https://instagram.com" target="_blank">
                            <InstagramIcon />
                        </IconButton>
                    </Box>
                </Toolbar>
            </Container>
        </AppBar>
    );
}
export default Footer;
